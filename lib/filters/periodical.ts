import * as slug from 'slug';

export function getSlugForName(name: string) {
  return slug(name)
    .toLowerCase()
    // Slugs should not be longer than UUIDs so we can differentiate:
    .substr(0, 35)
    // Remove ugly trailing dashes:
    .replace(/-+$/, '');
}
