import { InitialisedPeriodical } from '../../../../lib/interfaces/Periodical';
import { InitialisedScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

export async function fetchUserDashboard(
  database: Database,
  session: Session,
): Promise<[ InitialisedPeriodical[], InitialisedScholarlyArticle[] ]> {
  // tslint:disable-next-line:no-invalid-template-strings
  const periodicalAccountClause = (session.account) ? ' OR creator.creator=${accountId}' : '';
  // tslint:disable-next-line:no-invalid-template-strings
  const articleAccountClause = (session.account) ? ' AND (author.author=${accountId} OR author.author IS NULL)' : '';

  interface PeriodicalRow {
    identifier: string;
    name?: string;
    description?: string;
    headline?: string;
    date_published?: Date;
    creator?: string;
  }
  interface ArticleRow {
    identifier: string;
    name?: string;
    description?: string;
  }

  const result = await database.tx<[PeriodicalRow[], ArticleRow[]]>((t) => t.batch([
    t.manyOrNone(
      'SELECT'
        // tslint:disable-next-line:max-line-length
        + ' periodical.identifier, periodical.name, periodical.description, periodical.headline, periodical.date_published'
        + ' , creator.creator'
        + ' FROM periodicals periodical'
        + ' LEFT JOIN periodical_creators creator ON periodical.uuid=creator.periodical'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' WHERE periodical.creator_session=${sessionId}' + periodicalAccountClause
        + ' ORDER BY periodical.date_created DESC',
      {
        accountId: (session.account) ? session.account.identifier : undefined,
        sessionId: session.identifier,
      },
    ),
    t.manyOrNone(
      'SELECT'
        // tslint:disable-next-line:max-line-length
        + ' article.identifier, article.name, article.description'
        + ' FROM scholarly_articles article'
        + ' LEFT JOIN scholarly_article_authors author ON article.identifier=author.scholarly_article'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' WHERE article.creator_session=${sessionId}' + articleAccountClause
        + ' ORDER BY article.date_created DESC',
      {
        accountId: (session.account) ? session.account.identifier : undefined,
        sessionId: session.identifier,
      },
    ),
  ]));

  const periodicals: InitialisedPeriodical[] = result[0]
    .map((row) => {
      return {
        creator: (row.creator) ? { identifier: row.creator } : undefined,
        datePublished: (row.date_published) ? row.date_published.toISOString() : undefined,
        description: row.description,
        headline: row.headline,
        identifier: row.identifier,
        name: row.name,
      };
    });

  const articles: InitialisedScholarlyArticle[] = result[1]
    .map((row) => {
      return {
        description: row.description,
        identifier: row.identifier,
        name: row.name,
      };
    });

  return [ periodicals, articles ];
}
